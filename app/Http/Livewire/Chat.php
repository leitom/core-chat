<?php

namespace App\Http\Livewire;

use App\Models\Room;
use Livewire\Component;
use App\Events\MessageSent;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class Chat extends Component
{
    public $body = '';
    public $users = [];
    public $room = null;
    public $messages = [];

    public function mount(Room $room)
    {
        $this->room = $room;
        $this->messages = $this->room->messages()->with('user')->get()->toArray();
    }

    public function getListeners()
    {
        return [
            "echo-presence:chat.{$this->room->id},here" => 'here',
            "echo-presence:chat.{$this->room->id},joining" => 'joining',
            "echo-presence:chat.{$this->room->id},leaving" => 'leaving',
            "echo-private:chat.{$this->room->id},MessageSent" => 'newMessage',
        ];
    }

    public function here($users)
    {
        $this->users = $users;
    }

    public function joining($user)
    {
        $this->users = Collection::make($this->users)->push($user)->all();
    }

    public function leaving($user)
    {
        $this->users = Collection::make($this->users)->reject(fn ($existingUser) => $existingUser['id'] === $user['id'])->all();
    }

    public function newMessage($event)
    {
        $this->messages[] = $event['message'];
    }

    public function send()
    {
        $this->validate([
            'body' => ['required'],
        ]);

        $message = Auth::user()->messages()->create([
            'body' => $this->body,
            'room_id' => $this->room->id,
        ]);

        broadcast(new MessageSent($message))->toOthers();

        $this->messages[] = $message;

        $this->body = '';
    }

    public function render()
    {
        return view('livewire.chat');
    }
}

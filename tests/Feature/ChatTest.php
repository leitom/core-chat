<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Room;
use App\Models\User;
use Livewire\Livewire;
use App\Models\Message;
use App\Events\MessageSent;
use App\Http\Livewire\Chat;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ChatTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function body_is_required()
    {
        $this->actingAs($user = User::factory()->create());

        $room = Room::factory()->create(['name' => 'CoreTrek']);

        Livewire::test(Chat::class, [$room])
            ->set('body', '')
            ->call('send')
            ->assertHasErrors(['body' => ['required']]);

        $this->assertCount(0, Message::all());
    }

    /** @test */
    public function a_message_is_successfull_sent()
    {
        Event::fake();

        $this->actingAs($user = User::factory()->create());

        $room = Room::factory()->create(['name' => 'CoreTrek']);

        Livewire::test(Chat::class, [$room])
            ->set('body', 'This is a test')
            ->call('send');

        Event::assertDispatched(MessageSent::class);

        $this->assertDatabaseHas('messages', [
            'body' => 'This is a test',
            'user_id' => $user->id,
            'room_id' => $room->id,
        ]);
    }
}
